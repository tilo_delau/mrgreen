//
//  FirstTVC.swift
//  MrGreen
//
//  Created by IT-Högskolan on 02/03/16.
//  Copyright © 2016 tilo. All rights reserved.
//

import UIKit

class FirstTVC: UITableViewController {
    
    var jsonVoorhees: [NSDictionary]?
    var imgCache = [String:UIImage]()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    var isScrolling: Bool? = true
    var selectedImage = UIImage()
    
    var rowHeight: CGFloat = 44.00
    var currentSelectedIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        print("cell touched: ", cell?.textLabel?.text)
        selectedImage = (cell?.imageView?.image)!
        
        // Change height on select
       // tableView.beginUpdates()
        self.currentSelectedIndexPath = indexPath
       // tableView.endUpdates()
        //tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        performSegueWithIdentifier("FirstTVCtoDetailVC", sender: nil)
    }
    
    func doSerialization(data: NSData) {
        
        print("serialization")
        do{
            
            let json = try NSJSONSerialization.JSONObjectWithData(data, options:.AllowFragments)
            
            print("Json index 0: ", json[0])
            print("Json index 0 0: ", json[0]["name"])
            
            jsonVoorhees = json as? [NSDictionary]
            
            dispatch_async(dispatch_get_main_queue()) {
                print("trying to reload")
                self.tableView.reloadData()
                self.activityIndicator.stopAnimating()

            }
            
            if let stations = json["stations"] as? [[String: AnyObject]] {
                
                for station in stations {
                    
                    if let name = station["stationName"] as? String {
                        
                        if let year = station["buildYear"] as? String {
                            NSLog("%@ (Built %@)",name,year)
                        }
                        
                    }
                }                
            }
            
        }catch {
            print("Error with Json: \(error)")
            
        }
    }
    
    func fetchJSON() {
        
        /*
        Disable Transport Security (Don't do it for a real app!)
        Select/Create "App Transport Security Settings"
        Add "Allow Arbitrary Loads" and Change Boolean from NO to YES
        */
        
       // https://api.mrgreen.com/api/v2/interview/games?x-mrg-client-version=v1.10&x-mrg-client-type=ios
        
        let URL1 = "https://api.mrgreen.com/"
        let URL2 = "api/v2/interview/games?x-mrg-client-version=v1.10&x-mrg-client-type=ios"

        let requestURL: NSURL = NSURL(string: URL1+URL2)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestURL)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest) {
            (data, response, error) -> Void in
            
            let httpResponse = response as! NSHTTPURLResponse
            
            if (httpResponse.statusCode == 200) {
                print("Code 200: Good times! Json downloaded successfully.")

                if let data = data { // otherwise have to use data!
                    self.doSerialization(data)
                }
            }
        }
        task.resume() // Let's do this!
    }
    
    func setupIndicator() {
    activityIndicator.center = self.view.center
    
    activityIndicator.startAnimating()
    
    self.view.addSubview(activityIndicator)
    
    activityIndicator.startAnimating()
    
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("FirstTVC loaded")
        setupIndicator()
        fetchJSON()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if jsonVoorhees == nil { return 1 }
        
        print("count: ", jsonVoorhees!.count)
        return jsonVoorhees!.count
    }
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("stopped scrolling")
        isScrolling = false
    }
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        print("started scrolling")
        isScrolling = true
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        /*
        if indexPath.row == self.currentSelectedIndexPath.row {
            return 100
        } else { return 44 }
*/
        return 44
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath)

        // Configure the cell...
        
       cell.textLabel?.text = "Mr. Green is fetching data"
        
        if (jsonVoorhees == nil) {
            print("Jason Voorhees cannot die!")
            return cell
        }

        cell.textLabel?.text = jsonVoorhees![indexPath.row]["name"] as? String
        cell.imageView?.image = UIImage(named: "MrGreen712x534")
        
        //if isScrolling == true { return cell }
        
        if let img = imgCache[(jsonVoorhees![indexPath.row]["image"] as? String)!] {
            cell.imageView?.image = img
        } else {
            
            let url = NSURL(string: (jsonVoorhees![indexPath.row]["image"] as? String)!)!
            print("URL: ", url)
            
            let requestURL: NSURL = url
            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(URL: requestURL)
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: {
                (data, response, error) -> Void in
                
                let httpResponse = response as! NSHTTPURLResponse
                
                if (httpResponse.statusCode == 200) {
                    let img = UIImage(data: data!)
                    self.imgCache[(self.jsonVoorhees![indexPath.row]["image"] as? String)!] = img
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        print("index path row: ", indexPath.row)
                        if img != nil {
                        cell.imageView?.image = img
                        } else { print("img is nil!") }
                    })
                }
            })
            
            task.resume()
            
        }
        
        return cell
    }

    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FirstTVCtoDetailVC" {
            let target   = segue.destinationViewController as! DetailVC
            target.data  = jsonVoorhees!
            target.image = selectedImage
            
        }     }

}
